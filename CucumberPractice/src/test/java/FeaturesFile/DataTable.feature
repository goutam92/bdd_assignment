
Feature: Data Driven table test

@DataTableTest
Scenario: Verify that the new user registration is unsuccessful after passing incorrect inputs.
Given user navigates to Facebook
When user enter invalid data on the page
| Fields                 | Values              |
| First Name             | Tom                 |
| Last Name              | Kenny               |
| Email Address          | someone@someone.com |
Then the user registration should be unsuccessful
 

@DataMap
Scenario: Cucumber parameterization using Hash Map

Given user navigates to Facebook
And user enters values in Create account section
	| firstName  | lastName | number | password |
  | testuser_1 | Test@153 | 959495 | pwd1			|
  | testuser_2 | Test@154 | 000000 | pwd2			|
	| testuser_3 | Test@154 | 777777 | pwd3     |

@POJOClass
Scenario: Cucumber parameterization using POJO class object

Given user navigates to Facebook
And user enters values in Create account section using pojo
	| firstName  | lastName | number | password |
  | testuser_1 | Test@153 | 959495 | pwd1			|
  | testuser_2 | Test@154 | 000000 | pwd2			|
	| testuser_3 | Test@154 | 777777 | pwd3     |

#And I read the json data file "testData.json" 
