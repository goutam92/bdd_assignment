package cucumberTestRunner;


import org.junit.runner.RunWith;

import cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
@Cucumber.Options(features = { ".//FeaturesFile/DataTable.feature",
		".//FeaturesFile/LoginPage.feature"}, glue = {"stepDefination"},
		tags = { "~@DataTest","@SmokeTest" },
		format = {"pretty", "html:target/Reports"}
		//dryRun = true
		)

public class TestRunner {

}

//for more cucumber options , https://www.toolsqa.com/cucumber/cucumber-options/