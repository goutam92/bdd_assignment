package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.How;

public class LoginPage {

	public LoginPage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how =How.ID, using = "email")
	private WebElement textBoxUser;
	@FindBy(how =How.ID, using = "pass")
	private WebElement textBoxPassword;
	@FindBy(how =How.ID, using = "u_0_v")
	private WebElement buttonLogin;
	
	public WebElement getUserTextBox(){
		return textBoxUser;
	}
	public WebElement getTextBoxPassword(){
		return textBoxPassword;
	}
	public WebElement getLoginBtn(){
		return buttonLogin;
	}
}



