package stepDefination;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.annotation.After;
import cucumber.annotation.Before;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;
import cucumber.table.DataTable;
import gherkin.deps.com.google.gson.JsonArray;
import gherkin.deps.com.google.gson.JsonObject;
import pageObject.LoginPage;
import pageObject.UserData;

public class TestSteps {

	WebDriver driver = null;
	LoginPage loginPage = null;

	// Example of Hook
	@Before
	public void setUp() {
		System.out.println("Chrome set up");
		System.setProperty("webdriver.chrome.driver", "server/chromedriver.exe");
		driver = new ChromeDriver();
		loginPage = new LoginPage(driver);
	}

	@Given("^user navigates to Facebook$")
	public void goToFacebook() {
		System.out.println("Navigates to page");
		driver.navigate().to("https://www.facebook.com/");
	}

	@When("^User enter username as \"(.*)\"$")
	public void enterUsername(String arg1) {
		System.out.println("................");
		loginPage.getUserTextBox().sendKeys(arg1);
	}

	@When("^User enter password as \"(.*)\"$")
	public void enterPassword(String arg1) {
		loginPage.getTextBoxPassword().sendKeys(arg1);
		loginPage.getLoginBtn().click();
	}

	@Then("^login should be unsuccessful$")
	public void checkFail() {
		if (driver.getCurrentUrl().equalsIgnoreCase("https://www.facebook.com/login.php?login_attempt=1&lwv=110")) {
			System.out.println("Test1 Pass");
		} else {
			System.out.println("Login Failed");
		}
	}

	@When("^I enter Username as \"([^\"]*)\" and Password as \"([^\"]*)\"$")
	public void I_enter_Username_as_and_Password_as(String arg1, String arg2) {
		loginPage.getUserTextBox().sendKeys(arg1);
		loginPage.getTextBoxPassword().sendKeys(arg2);
		loginPage.getLoginBtn().click();
	}

	@When("^user enter invalid data on the page$")
	public void enterData(DataTable table) {
		// Initialize data table
		List<List<String>> data = table.raw();
		System.out.println(data.get(1).get(1) + " Data");

		// Enter data
		driver.findElement(By.name("firstname")).sendKeys(data.get(1).get(1));
		driver.findElement(By.name("lastname")).sendKeys(data.get(2).get(1));
		driver.findElement(By.name("reg_email__")).sendKeys(data.get(3).get(1));
	}

	@Then("^the user registration should be unsuccessful$")
	public void User_registration_should_be_unsuccessful() {
		if (driver.getCurrentUrl().equalsIgnoreCase("https://www.facebook.com/")) {
			System.out.println("registration unsuccessful");
		} else {
			System.out.println("Test Failed");
		}
	}

	@When("^I read the json data file \"(.*?)\"$")
	public void i_read_the_json_data_file(String arg1) throws Throwable {
		FileInputStream fin = new FileInputStream(new File(System.getProperty("user.dir") + "//resource//" + arg1));
		InputStreamReader in = new InputStreamReader(fin);
		BufferedReader bufferedReader = new BufferedReader(in);
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			sb.append(line);
		}
		String json = sb.toString();
		System.out.println(json + "*************");

		////////////////////////////

	}

	@When("^user enters values in Create account section$")
	public void enterDataUsingHashMap(DataTable table) {
		for (Map<String, String> data : table.asMaps(String.class, String.class)) {
			driver.findElement(By.name("firstname")).sendKeys(data.get("firstName"));
			driver.findElement(By.name("lastname")).sendKeys(data.get("lastName"));
			driver.findElement(By.name("reg_email__")).sendKeys(data.get("number"));
			driver.findElement(By.name("reg_passwd__")).sendKeys(data.get("pwd"));
		}
	}

	@When("^user enters values in Create account section using pojo$")
	public void enterDataUsingPOJO(List<UserData> userDatas) {

		for (UserData data : userDatas) {
			driver.findElement(By.name("firstname")).sendKeys(data.getFirstName());
			driver.findElement(By.name("lastname")).sendKeys(data.getLastName());
			driver.findElement(By.name("reg_email__")).sendKeys(data.getNumber());
			driver.findElement(By.name("reg_passwd__")).sendKeys(data.getPassword());
		}

	}

	@After
	public void closeBrowser() {
		driver.close();
	}

}
